<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request){
        $authUser = User::query()->where('email',$request->email)->first();

        if($authUser){
            $authUser->avatar = $request->avatar;
            $authUser->save();
            Auth::login($authUser,true);
            return response()->json([],200);
        }else{
            return response()->json([],422);
        }
    }
}
