<?php

namespace App\Http\Controllers\Dashboard\Forms;

use App\Http\Controllers\Controller;
use App\Models\BudgetLine;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;

class ExpenseFormController extends Controller
{
    public function index(){
        ///
       $project = Project::all();
       $budget = BudgetLine::all();
        $approver = User::all();
        $currency = config('currency');
        return view('dashboard.forms.expense',
        [
            'budget'=>$budget,
            'project'=>$project,
            'approver'=>$approver,
            'currency'=>$currency
        ]);
    }
}
