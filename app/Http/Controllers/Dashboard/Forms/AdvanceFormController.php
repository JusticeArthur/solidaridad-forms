<?php

namespace App\Http\Controllers\Dashboard\Forms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdvanceFormController extends Controller
{
    public function index(){
        return view('dashboard.forms.advance-form.index');
    }

    public function createForm(){
        return view('dashboard.forms.advance-form.create');
    }
}
