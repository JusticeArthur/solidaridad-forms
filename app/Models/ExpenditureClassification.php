<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpenditureClassification extends Model
{
    protected $guarded = [];
}
