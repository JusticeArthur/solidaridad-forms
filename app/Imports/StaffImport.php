<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StaffImport implements ToCollection,WithHeadingRow
{

    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            try{
                User::create(
                    [
                        'first_name' => trim($row['first_name_required']),
                        'last_name' => trim($row['last_name_required']),
                        'email' => trim($row['email_address_required']),
                        'status' => strtolower(trim($row['status_read_only']))
                    ]
                );
            }catch (\Exception $exception){}
        }
    }
}
