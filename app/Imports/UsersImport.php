<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class UsersImport implements  WithMultipleSheets
{
    protected $table = 'users';
    function __construct($table = 'users')
    {
        $this->table = $table;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        return [
            'list of projects&codes' => new ProjectSheet(),
            'Budget Lines' => new BudgetSheet(),
            'COA' => new ExpenditureClassificationSheet(),
            'staff list' => new UserSheet($this->table),

        ];
    }
}
