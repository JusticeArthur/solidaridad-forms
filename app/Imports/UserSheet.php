<?php


namespace App\Imports;


use App\Models\Country;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UserSheet  implements ToCollection, WithHeadingRow
{

    protected $table;
    function __construct($table = 'users')
    {
        $this->table = $table;
    }

    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        switch ($this->table){
            case 'users':
                //dd($collection);
                $users = User::all();
                //dd($users->toArray());
                foreach ($collection as $item){
                   // dd($item);
                    foreach ($users as $user){
                        $country = null;
                        if($item['country']){
                            $country = Country::where('code',$item['country'])->first()->id;
                        }
                        if(Str::containsAll(strtolower($user->name),explode(' ',strtolower(trim($item['staff']))))){

                            if(strlen(trim($item['gender'][0]) > 0)){
                                $user->gender = strtoupper(trim($item['gender'][0]));
                            }
                            $user->role = trim($item['status']);
                            $user->position = trim($item['position']);
                            $user->location = trim($item['location']);
                            $user->country_id = $country;
                            $user->save();
                            try{

                            }catch (\Exception $exception){}
                        }
                    }
                }
                break;
        }
        // TODO: Implement collection() method.
    }
}
