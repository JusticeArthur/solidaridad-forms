<?php


namespace App\Imports;


use App\Models\Project;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProjectSheet  implements ToCollection,WithHeadingRow
{


    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        foreach ($collection as $item){
            $name = trim($item['informal_name']);
            $code = trim($item['project_number']);
            $title = str_replace('('.$name.')','',$item['project_project_title']);
            $title = str_replace($code,'',$title);
            $title = str_replace(':','',$title);
            try{
                Project::create([
                    'code' => $code,
                    'name' => $name,
                    'title' => trim($title),
                    'commodity' => trim($item['commoditiesprogrmme']),
                    'start_date' => trim($item['start_date']),
                    'end_date' => trim($item['end_date'])
                ]);
            }catch (\Exception $e){}

        }
    }
}
