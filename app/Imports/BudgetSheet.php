<?php


namespace App\Imports;


use App\Models\BudgetLine;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Project;

class BudgetSheet implements ToCollection
{


    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        //dd((int)$collection[28][1]);
        foreach ($collection as $item) {
            $project = null;
            if($item[1]){
                $project = Project::query()->where('code',(int)trim($item[1]))->first();
            }else{
                continue;
            }
            try {
                if($item[2]){
                    $name = trim($item[2]);
                    $code = (int)trim($item[1]);
                    $pattern = "/{$code}\w+\:/i";

                    preg_match($pattern, $name, $matches, PREG_OFFSET_CAPTURE);

                    if(count($matches) < 1){
                        continue;
                    }
                    $title = str_replace($matches[0],'',$name);
                    BudgetLine::create([
                        'name' => $title,
                        'project_id' => $project ? $project->id : $project
                    ]);
                }

            } catch (\Exception $e) {
            }
        }
        // TODO: Implement collection() method.
    }
}
