<?php


namespace App\Imports;


use App\Models\ExpenditureClassification;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ExpenditureClassificationSheet implements ToCollection
{


    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        foreach ($collection as $item) {
            try {
                if ($item[0])
                    ExpenditureClassification::create([
                        'name' => trim($item[0])
                    ]);
            } catch (\Exception $e) {
            }
        }
        // TODO: Implement collection() method.
    }
}
