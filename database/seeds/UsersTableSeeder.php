<?php

use App\Imports\StaffImport;
use App\Imports\UsersImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path(). '\\files\\expense.xlsx';
        $staff = public_path(). '\\files\\staff.xlsx';
        Excel::import(new StaffImport, $staff);
        Excel::import(new UsersImport, $path);
    }
}
