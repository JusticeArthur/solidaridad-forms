<?php

use App\Models\Rec;
use Illuminate\Database\Seeder;

class RecTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $recs = [
          'WAF',
          'SAF',
          'ECA'
        ];
        foreach ($recs as $rec){
            try{
                Rec::create([
                    'name' => $rec
                ]);
            }catch (Exception $e){}

        }
    }
}
