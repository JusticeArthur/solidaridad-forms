<?php

use App\Models\Country;
use App\Models\Rec;
use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(count(Country::all()) <= 0 ){
            $countries = json_decode(file_get_contents(public_path() . '/files/countries.json'),true);
            foreach ($countries as $country){
                try{
                    Country::firstOrCreate([
                        'name' => $country['countryName'],
                        'code' => $country['countryShortCode']
                    ]);
                }catch (Exception $e){
                    continue;
                }
            }
            //
            //temp
            $testCountries = Country::all();
            $withCodes = json_decode(file_get_contents(public_path() . '/files/codes.json'),true);
            foreach ($testCountries as $country){
                foreach ($withCodes as $withCode){
                    if(strtolower($country['code']) === strtolower($withCode['code'])){
                        $country['dial_code'] = $withCode['dial_code'];
                        try{
                            $country->save();
                        }catch (\Exception $e){

                        }
                    }
                }
            }

            //save recs

        }

        $recs = [
            [
                'name' => 'WAF',
                'countries' => [
                    'NG',
                    'GH',
                    'LR',
                    'SL',
                    'CI'
                ]
            ]
        ];
        foreach ($recs as $rec){
            $finder = Rec::where('name',$rec['name'])->first();
            if($finder){
                $countries = Country::whereIn('code',$rec['countries'])->get();
                foreach ($countries as $country){
                    $country->rec_id = $finder->id;
                    $country->save();
                }
            }
        }

    }
}
