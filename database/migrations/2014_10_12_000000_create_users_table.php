<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('role')->nullable();
            $table->string('position')->nullable();
            $table->string('email')->unique()->nullable();
            $table->enum('gender',['M','F'])->nullable();
            $table->string('location')->nullable();
            $table->string('grade')->nullable();
            $table->bigInteger('country_id')->nullable();
            $table->bigInteger('project_id')->nullable();
            $table->string('status')->default('active');
            $table->string('avatar')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
