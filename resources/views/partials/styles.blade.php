<link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/apple-touch-icon.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{asset('images/logo/fav-icon-32-32.jpg')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/logo/fav-icon-16-16.jpg')}}">
<!-- Stylesheet -->
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/css/base/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/css/base/elisyam-1.5.css')}}">

