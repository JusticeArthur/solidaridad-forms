<!DOCTYPE html>
<!--
Item Name: Elisyam - Web App & Admin Dashboard Template
Version: 1.5
Author: SAEROX

** A license must be purchased in order to legally use this template for your project **
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Solidaridad Forms | @yield('title')</title>
    <meta name="description" content="Elisyam is a Web App and Admin Dashboard Template built with Bootstrap 4">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Google Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!-- Favicon -->
    @include('partials.styles')
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body id="page-top">
<!-- Begin Preloader -->
@include('partials.loader')
<!-- End Preloader -->
<div id="app" class="page db-modern">
    <!-- Begin Header -->
    <header class="header">
        @include('partials.header')
    </header>
    <!-- End Header -->
    <!-- Begin Page Content -->
    <div class="page-content">
        <!-- Begin Navigation -->
        @include('partials.navigation')
        <!-- End Navigation -->
        <div class="content-inner boxed mt-4 w-100">
            <div class="container">
                @yield('content')

            </div>
            <!-- End Container -->
            <!-- Begin Page Footer-->
            @include('partials.footer')
            <!-- End Page Footer -->
            <a href="#" class="go-top"><i class="la la-arrow-up"></i></a>

        </div>
        <!-- End Content -->
    </div>
    <!-- End Page Content -->
</div>
<!-- Begin Vendor Js -->
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('assets/vendors/js/base/core.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/nicescroll/nicescroll.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/waypoints/waypoints.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/chart/chart.min.js')}}"></script>
<script src="{{asset('assets/vendors/js/progress/circle-progress.min.js')}}"></script>

<script src="{{asset('assets/vendors/js/app/app.min.js')}}"></script>
<script src="{{asset('assets/js/dashboard/db-modern.min.js')}}"></script>
<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
<!-- End Page Snippets -->
</body>
</html>
