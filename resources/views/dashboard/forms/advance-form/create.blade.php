@extends('layouts.app')
@section('title','Create Advance Form')

@section('content')
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">Create Advance Form</h2>
                {{--<div>
                    <div class="page-header-tools">
                        <a class="btn btn-gradient-01" href="#">Add Form</a>
                    </div>
                </div>--}}
            </div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Begin Row -->
    <div class="widget">
        <div class="widget-body">
            <create-advance-form></create-advance-form>
        </div>
    </div>
    <div style="margin-bottom: 20px;"></div>
@endsection
