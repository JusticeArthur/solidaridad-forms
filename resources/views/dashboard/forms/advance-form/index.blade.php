@extends('layouts.app')
@section('title','Advance Form')

@section('content')
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">Advance Forms</h2>
                <div>
                    <div class="page-header-tools">
                        <a class="btn btn-gradient-01" href="{{route('forms.advance.create')}}">Add Form</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Begin Row -->
    <div class="row flex-row">
        <div class="col-xl-6 col-md-6 os-animation" data-os-animation="fadeInUp">
            <div class="widget widget-21 has-shadow">
                <div class="widget-body h-100 d-flex align-items-center">
                    <div class="section-title">
                        <h3>Acceptance Rate</h3>
                    </div>
                    <div class="hit-rate">
                        <div class="percent"></div>
                    </div>
                    <div class="value-progress text-green">
                        + 34%
                    </div>
                </div>
            </div>
            <div class="widget widget-22 bg-gradient-03">
                <div class="widget-body h-100 d-flex align-items-center">
                    <div class="section-title">
                        <h3>Happy Customers</h3>
                    </div>
                    <div class="happy-customers">
                        <div class="percent"></div>
                    </div>
                    <div class="value-progress">
                        + 54 Today
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-md-6 os-animation" data-os-animation="fadeInUp">
            <!-- Begin Widget 04 -->
            <div class="widget widget-04 has-shadow">
                <!-- Begin Widget Header -->
                <div class="widget-header bordered d-flex align-items-center">
                    <h2>Post</h2>
                    <div class="widget-options">
                        <div class="dropdown">
                            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                    class="dropdown-toggle">
                                <i class="la la-ellipsis-h"></i>
                            </button>
                            <div class="dropdown-menu">
                                <a href="#" class="dropdown-item">
                                    <i class="la la-check"></i>Valid Post
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="la la-edit"></i>Edit Widget
                                </a>
                                <a href="faq.html" class="dropdown-item faq">
                                    <i class="la la-question-circle"></i>FAQ
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Widget Header -->
                <!-- Begin Widget Body -->
                <div class="widget-body p-0">
                    <div class="post-container">
                        <div class="media mb-3">
                            <div class="media-left align-self-center user">
                                <a href="pages-profile.html"><img src="assets/img/avatar/avatar-07.jpg"
                                                                  class="rounded-circle" alt="..."></a>
                            </div>
                            <div class="media-body align-self-center ml-3">
                                <div class="title">
                                    <span class="username">Lisa Garett</span> posted an image
                                </div>
                                <div class="time">42 min ago</div>
                            </div>
                        </div>
                        <img src="assets/img/background/01.jpg" alt="..." class="img-fluid">
                        <div class="col no-padding d-flex justify-content-end mt-3">
                            <div class="meta">
                                <ul>
                                    <li><a href="#"><i class="la la-comment"></i><span class="numb">38</span></a></li>
                                    <li><a href="#"><i class="la la-heart"></i><span class="numb">94</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Begin Write Comment -->
                    <div class="input-group mt-5">
                        <input type="text" class="form-control pr-0" placeholder="Write a comment ...">
                        <span class="input-group-addon">
                                                <button class="btn" type="button">
                                                    <i class="la la-smile-o la-2x"></i>
                                                </button>
                                            </span>
                        <span class="input-group-addon">
                                                <button class="btn pr-3" type="button">
                                                    <i class="la la-pencil la-2x"></i>
                                                </button>
                                            </span>
                    </div>
                    <!-- End Write Comment -->
                </div>
                <!-- End Widget Body -->
            </div>
            <!-- End Widget 04 -->
        </div>
    </div>
@endsection
