/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');

window.Vue = require('vue');

import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
//import 'vue-material/dist/theme/default.css';

import GAuth from 'vue-google-oauth2';
import Toastr from "vue-toastr";
Vue.use(Toastr);
Vue.use(GAuth, {
    clientId: '521060493861-b2kp3dh04dmbe5mujfi4q342kdo4hf6e.apps.googleusercontent.com',
    scope: 'profile email',
    prompt: 'select_account'
});
Vue.use(VueMaterial);
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('app-login',require('./components/auth/login.vue').default);
Vue.component('form-expense',require('./components/forms/expense.vue').default);
Vue.component('create-advance-form',require('./components/forms/advance/create-advance.vue').default);
Vue.component('advance-form-index',require('./components/forms/advance/index.vue').default);


const app = new Vue({
    el: '#app',
});
