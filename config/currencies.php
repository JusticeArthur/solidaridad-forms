<?php

return [
    'currencies' => [
        'GH¢',
        'Naira',
        'Euro',
        'USD',
        'CFA',
        'SLL',
        'LD'
    ]
];
