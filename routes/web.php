<?php

Route::get('/', function () {
    return view('welcome');
})->name('login');

Route::post('/login','Auth\AuthController@login');

Route::group(['middleware' => ['auth']],function (){
    Route::namespace('Dashboard')->group(function (){
        Route::get('dashboard','DashboardController@index')->name('dashboard');
        Route::namespace('Forms')->prefix('forms')->name('forms.')->group(function (){
            Route::get('expense-form','ExpenseFormController@index')->name('expense');
            Route::prefix('advance-form')->name('advance.')->group(function (){
                Route::get('','AdvanceFormController@index')->name('index');
                Route::get('create','AdvanceFormController@createForm')->name('create');
            });
        });
    });
});
